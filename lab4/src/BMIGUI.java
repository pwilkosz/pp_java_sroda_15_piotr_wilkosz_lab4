//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

//import LicznikGlosow.PlacekButtonListener;

public class BMIGUI
{
   private int WIDTH = 300;
   private int HEIGHT = 250;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel, klasyfikacja;
   private JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w centymetrach:");
      wagaLabel = new JLabel ("Twoja waga w kilokramach: ");
      klasyfikacja = new JLabel("");
      //stworz etykiete "to jsest twoje BMI" 
      BMILabel = new JLabel("Twoje BMI:");
      //stworz etykiete wynik dla wartosci BMI
      wynikLabel = new JLabel("");
      // stworz JTextField dla wzrostu
      wzrost = new JTextField("",20);
      // stworz JTextField dla wagi
      waga = new JTextField("",20);
      

      // stworz przycisk, ktory po wcisnieciu policzy BMI
      oblicz = new JButton("Oblicz BMI");
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz.addActionListener (new BMIListener());
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.yellow);
      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      panel.add(wzrostLabel); panel.add(wzrost);
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      panel.add(wagaLabel); panel.add(waga);
      //dodaj do panelu przycisk
      panel.add(oblicz);
      //dodaj do panelu etykiete BMI
      panel.add(BMILabel);
      //dodaj do panelu etykiete dla wyniku
      panel.add(wynikLabel);
      panel.add(klasyfikacja);
      //dodaj panel do frame 
      frame.getContentPane().add (panel);
   }
   
   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
      public void actionPerformed (ActionEvent event)
      {
         String wzrostText, wagaText;
         double wzrostVal, wagaVal;
         double bmi;

	 //pobierz tekst z pol tekstowych dla wagi i wzrostu
     wzrostText = wzrost.getText();
     wagaText = waga.getText();
	 //Wykorzystaj Integer.parseInt do konwersji tekstu na integer
     wzrostVal = Double.parseDouble(wzrostText);
     wagaVal = Double.parseDouble(wagaText);
	 //oblicz  bmi = waga / (wzrost)^2
     bmi = wagaVal/(Math.pow((wzrostVal/100),2));
	 //Dodaj wynik do etykiety dla wyniku. 
	 // Wykorzystaj Double.toString do konwersji double na string.
     
     wynikLabel.setText(Double.toString(bmi));
     //ustaw klasyfikacje wg BMI
     String ocena;
     
     if(Double.compare(bmi, 19) < 0){
    	 ocena = "Chudy";
     }
     else if(Double.compare(bmi, 25) <= 0){
    	 ocena = "OK";
     }
     else if(Double.compare(bmi, 30) <= 0){
    	 ocena = "za ci�ki";
     }
     else ocena = "GRUBY";
     klasyfikacja.setText("Jestes: " + ocena);
     //repaint ();

      }
   }
}

